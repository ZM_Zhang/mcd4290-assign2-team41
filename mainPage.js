//Feature 10: Display all the routes

//on page load, fetch routes from localstorage and display them
window.addEventListener("load", (event) => {
  showRoutes();
});

//function to fetch the routes
function showRoutes() {
  var routes = routeList;
  console.log(routes);

  var list = document.getElementById("routeList");
  var table = document.createElement("table");

  //sort routes based on date
  routes.sort(compareDates);

  //display the data in a table
  for (let i = 0; i < routes.length; i++) {
    const el = routes[i];

    var tr = document.createElement("tr");

    var td1 = document.createElement("td");

    var ul = document.createElement("ul");

    var item = document.createElement("li");
    item.appendChild(
      document.createTextNode("Route Name: " + routes[i].name + " ")
    );

    var item2 = document.createElement("li");
    item2.appendChild(document.createTextNode("Ship: " + routes[i].ship + " "));

    var item3 = document.createElement("li");
    item3.appendChild(
      document.createTextNode("Source: " + routes[i].srcPort + " ")
    );

    var item4 = document.createElement("li");
    item4.appendChild(
      document.createTextNode("Destination: " + routes[i].destPort + " ")
    );

    var item5 = document.createElement("li");
    item5.appendChild(
      document.createTextNode("Distance: " + routes[i].dist + " ")
    );

    var item6 = document.createElement("li");
    item6.appendChild(document.createTextNode("Time: " + routes[i].time + " "));

    var item7 = document.createElement("li");
    item7.appendChild(document.createTextNode("Cost: " + routes[i].cost + " "));

    var item8 = document.createElement("li");
    item8.appendChild(
      document.createTextNode("Date: " + routes[i].startDate + " ")
    );
      
    ul.appendChild(item);
    ul.appendChild(item2);
    ul.appendChild(item3);
    ul.appendChild(item4);
    ul.appendChild(item5);
    ul.appendChild(item6);
    ul.appendChild(item7);
    
    td1.appendChild(ul);

    tr.appendChild(td1);

    table.appendChild(tr);
  }

  list.appendChild(table);
    
  //fetch route from routelist
  var route = routeList.find((x) => x.name === name);

  //fetch ship from shiplist
  var ship = shipList.find((x) => x.name === route.ship);