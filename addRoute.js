//Feature 7: Create a Route

//Feature 8: Incorporate the API

//when the window has finished loading, fetch the data and display it
window.addEventListener("load", (event) => {
  document.getElementById("ports").appendChild(fetchPorts("source"));
  document.getElementById("dests").appendChild(fetchPorts("dest"));

  document.getElementById("ships").appendChild(fetchShips());
  fetchShipsFromApi();
  fetchPortsFromApi();
});

//save the route
function saveRoute() {
  var ship = document.getElementById("ship_sel").value;
  var src = document.getElementById("src_sel").value;
  var dest = document.getElementById("dst_sel").value;
  var date = document.getElementById("sdate").value;
  var name = document.getElementById("name").value;

  console.log(date);

  //validate data
  if (validateForm(ship, src, dest, date, name)) {
    var portsFromApi = JSON.parse(localStorage.getItem("portsFromApi"));
    Array.prototype.push.apply(portList, portsFromApi);

    var shipsFromApi = JSON.parse(localStorage.getItem("shipsFromApi"));
    Array.prototype.push.apply(shipList, shipsFromApi);

    //get source port
    var srcPort = portList.filter((port) => {
      return port.name === src;
    });

    //get destination port
    var destPort = portList.filter((port) => {
      return port.name === dest;
    });

    //get ships
    objIndex = shipList.findIndex((obj) => obj.name == ship);

    var distanceBtnPorts = Math.abs(
      getDistanceFromLatLonInKm(
        srcPort[0].lat,
        srcPort[0].lng,
        destPort[0].lat,
        destPort[0].lng
      )
    );

    //check if the ship has a distance greater than route distance
    if (distanceBtnPorts <= shipList[objIndex].range) {
      shipList[objIndex].status = "En-Route";

      addMarkerToMap(srcPort[0].lat, srcPort[0].lng);
      addMarkerToMap(destPort[0].lat, destPort[0].lng);

      const myRoute = Object.create(Route);

      myRoute.ship = ship;
      myRoute.srcPort = src;
      myRoute.destPort = dest;
      myRoute.date = date;
      myRoute.name = name;
      myRoute.wayPointList = [];

      myRoute.dist = distanceBtnPorts.toFixed(2);
      myRoute.time = (distanceBtnPorts / shipList[objIndex].maxSpeed).toFixed(
        2
      );
      myRoute.cost = (distanceBtnPorts * shipList[objIndex].cost).toFixed(2);

      routeList.push(myRoute);

      localStorage.setItem("routes", JSON.stringify(routeList));

      window.location.replace("index.html");
    } else {
      alert("Select a ship with a greater range");
    }
  }
}

//validates the data entered
function validateForm(ship, src, dest, date, name) {
  //checks if the name has been input
  if (name.length == 0) {
    alert("Please enter a route name!");
    return false;
  }

  //checks if the source port and destination port are the same
  if (src === dest) {
    alert("Source cannot be same as destination!");
    return false;
  }

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();

  var date1 = yyyy + "-" + mm + "-" + dd;
  var date2 = date;

  date1 = new Date(date1);
  date2 = new Date(date2);

  //checks if the date is a present or future date
  if (date2 < date1) {
    alert("Please select today or a future date!");
    return false;
  }

  return true;
}

//adds marker to the map
function addMarkerToMap(lat, lng) {
  var marker = new mapboxgl.Marker().setLngLat([lng, lat]).addTo(map);
}

//fetch ships from localstorage and display them
function fetchShips() {
  var myShips = shipList;

  var select = document.createElement("select");
  select.id = "ship_sel";

  var optgroup = document.createElement("optgroup");
  optgroup.setAttribute("label", "User Ships");

  for (let i = 0; i < myShips.length; i++) {
    const el = myShips[i];

    var op = document.createElement("option");
    op.value = el.name;
    op.appendChild(document.createTextNode(el.name + " "));

    optgroup.appendChild(op);
  }

  select.appendChild(optgroup);

  return select;
}

//fetch ships from api and display them
async function fetchShipsFromApi() {
  var select = document.getElementById("ship_sel");

  var url = url_ships;

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      console.log(data.ships);
      var ships = data.ships;

      localStorage.setItem("shipsFromApi", JSON.stringify(data.ships));

      var optgroup = document.createElement("optgroup");
      optgroup.setAttribute("label", "API Ships");

      for (var i = 0; i < ships.length; i++) {
        ships[i].author = "api";
        const el = ships[i];

        var op = document.createElement("option");
        op.value = el.name;
        op.appendChild(document.createTextNode(el.name + " "));

        optgroup.appendChild(op);
      }

      select.appendChild(optgroup);
    });
}

//fetch ports from localstorage and display them
function fetchPorts(type) {
  var myPorts = portList;

  var select = document.createElement("select");
  if (type === "source") {
    select.id = "src_sel";
  } else if (type === "dest") {
    select.id = "dst_sel";
  }

  var optgroup = document.createElement("optgroup");
  optgroup.setAttribute("label", "User Ports");

  for (let i = 0; i < myPorts.length; i++) {
    const el = myPorts[i];

    var op = document.createElement("option");
    op.value = el.name;
    op.appendChild(document.createTextNode(el.name + " "));
    optgroup.appendChild(op);
  }

  select.appendChild(optgroup);

  return select;
}

//fetch ports from api and display them
async function fetchPortsFromApi() {
  var select1 = document.getElementById("src_sel");
  var select2 = document.getElementById("dst_sel");

  var url = url_ports;

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      console.log(data.ports);
      var ports = data.ports;

      var ports = ports.slice(0, 20);

      localStorage.setItem("portsFromApi", JSON.stringify(ports));

      var optgroup1 = document.createElement("optgroup");
      optgroup1.setAttribute("label", "API Ports");

      for (var i = 0; i < ports.length; i++) {
        ports[i].author = "api";
        const el = ports[i];

        var op = document.createElement("option");
        op.value = el.name;
        op.appendChild(document.createTextNode(el.name + " "));

        optgroup1.appendChild(op);
      }

      select1.appendChild(optgroup1);

      var optgroup2 = document.createElement("optgroup");
      optgroup2.setAttribute("label", "API Ports");

      for (var i = 0; i < ports.length; i++) {
        ports[i].author = "api";
        const el = ports[i];

        var op = document.createElement("option");
        op.value = el.name;
        op.appendChild(document.createTextNode(el.name + " "));

        optgroup2.appendChild(op);
      }

      select2.appendChild(optgroup2);
    });
}

//calculate distance in kms given coordinates
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

//degrees to radians
function deg2rad(deg) {
  return deg * (Math.PI / 180);
}