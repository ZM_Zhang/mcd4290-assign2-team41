//Feature 9: Add waypoints to a route

//Feature 11: View a route

//Feature 12: Delete a route

var routeInfo;
var glRouteName;

//on page load fetch dat for route and display it
window.addEventListener("load", (event) => {
  //route is gotten from the current url
  var route = window.location.search.substr(1);
  var tmp = route.split("=");
  routeInfo = fetchRouteData(tmp[1]);
  glRouteName = tmp[1];
});

//fetch route data from localstorage
function fetchRouteData(name) {
  var routes = JSON.parse(localStorage.getItem("routes"));

  var data = routes.find((x) => x.name === name);

  var ul = document.createElement("ul");

  var item1 = document.createElement("li");
  item1.appendChild(document.createTextNode("Route Name: " + data.name));

  var item6 = document.createElement("li");
  item6.appendChild(document.createTextNode("Distance: : " + data.dist));

  var item7 = document.createElement("li");
  item7.appendChild(document.createTextNode("Time: : " + data.time));

  var item8 = document.createElement("li");
  item8.appendChild(document.createTextNode("Cost: : " + data.cost));

  var delBtn = document.createElement("button");
  delBtn.type = "button";
  delBtn.innerHTML = "Delete Route";
  delBtn.className =
    "mdl-button mdl-js-button mdl-button--raised mdl-button--colored paddedBtn";

  delBtn.onclick = function () {
    if (window.confirm("Are you sure you want to delete this route?")) {
      console.log("deleted");
      deleteRoute(data.name);
    }
  };

  var pstBtn = document.createElement("button");
  pstBtn.type = "button";
  pstBtn.innerHTML = "Postpone Route";
  pstBtn.className =
    "mdl-button mdl-js-button mdl-button--raised mdl-button--colored paddedBtn";

  pstBtn.onclick = function () {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + "/" + mm + "/" + yyyy;

    var prt = prompt("Please enter the new date:", today);
    if (prt == null || prt == "") {
      console.log("cancelled");
    } else {
      postponeRoute(data.name, today);
    }
  };

  ul.appendChild(item1);

  ul.appendChild(item6);
  ul.appendChild(item7);
  ul.appendChild(item8);

  ul.appendChild(delBtn);
  ul.appendChild(pstBtn);

  var rd = document.getElementById("routeData");
  rd.appendChild(ul);

  //display way point on the map
  map.addSource("route", {
    type: "geojson",
    data: {
      type: "Feature",
      properties: {},
      geometry: {
        type: "LineString",
        coordinates: data.wayPointList,
      },
    },
  });
  map.addLayer({
    id: "route",
    type: "line",
    source: "route",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#FFA500",
      "line-width": 7,
    },
  });

  return data;
}

//function to add waypoint to route
function addWayPoint(lngLat) {
  var coords = [lngLat.lng, lngLat.lat];

  ind = routeList.findIndex((obj) => obj.name == glRouteName);

  var wpList = routeList[ind].wayPointList;
  var distanceBtnPoints;

  if (wpList.length > 1) {
    distanceBtnPoints = Math.abs(
      getDistanceFromLatLonInKm(
        wpList[wpList.length - 1].lat,
        wpList[wpList.length - 1].lng,
        wpList[wpList.length - 2].lat,
        wpList[wpList.length - 2].lng
      )
    );

    if (distanceBtnPoints < 100) {
      alert("Select a waypoint at least 100kms away!");
    } else {
      routeList[ind].wayPointList.push(coords);
    }
  } else {
    routeList[ind].wayPointList.push(coords);
  }

  localStorage.setItem("routes", JSON.stringify(routeList));

  console.log(lngLat);

  //display way point on the map
  map.addSource("route", {
    type: "geojson",
    data: {
      type: "Feature",
      properties: {},
      geometry: {
        type: "LineString",
        coordinates: routeList[ind].wayPointList,
      },
    },
  });
  map.addLayer({
    id: "route",
    type: "line",
    source: "route",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#FFA500",
      "line-width": 7,
    },
  });
}

//function to postpone the route
function postponeRoute(name, date) {
  var rt = routeList;

  var ind = rt.findIndex((r) => r.name == name);

  rt[ind].date = date;

  localStorage.setItem("routes", JSON.stringify(rt));
}

//function to delete the route
function deleteRoute(name) {
  //fetch ships from api and combine with user ships
  var shipsFromApi = JSON.parse(localStorage.getItem("shipsFromApi"));
  Array.prototype.push.apply(shipList, shipsFromApi);

  var filtered;

  //select route
  var route = routeList.find((x) => x.name === name);

  //select ship
  var ship = shipList.find((x) => x.name === route.ship);

  //confirm ship is not en-route
  if (ship.status.toLowerCase() !== "en-route") {
    filtered = routeList.filter(function (el) {
      return el.name != name;
    });
  } else {
    alert("Ship is en-route!");
  }

  routeList = filtered;
  localStorage.setItem("routes", JSON.stringify(routeList));

  window.location.replace("index.html");
}

//fucntion to calculate distance in kms given coordinates
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad function below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

//degrees to radians
function deg2rad(deg) {
  return deg * (Math.PI / 180);
}