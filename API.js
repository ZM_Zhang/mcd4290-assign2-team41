let flightsListElement = document.getElementById('flights-list');
let routes = [];

// Make the request
let data = {
    q: "QF",
    key: "LAX",
    callback: "routesResponse"
};
jsonpRequest("https://api.opencagedata.com/geocode/v1/json?q=PLACENAME&key=YOUR-API-KEY", data);

function jsonpRequest(url, data)
{
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

function routesResponse(routesArray)
{
    var i
     var listHTML = "";
   for (i = 0; i < routesArray.length;i++)
 {
     routes = routesArray;

    // List view section heading: Flight list
   
    listHTML += "<tr> <td onmousedown=\"listRowTapped("+i+")\" class=\"full-width mdl-data-     table__cell--non-numeric\">" + routesArray[i].sourceAirport + " &rarr; " + routesArray[i].destinationAirport;
    listHTML += "<div class=\"subtitle\">" + routes[i].destinationAirportId + ", Stops: " + routes[i].stops +"       </div></td></tr>";
 
    flightsListElement.innerHTML = listHTML;

}

function airportResponse(airport)
{
    let message = "Name: " + airport.name + "\n";
    message += "Location: " + airport.city + ", " + airport.country;
    alert(message);
}

function listRowTapped(routeIndex)
{
    console.log(routes[routeIndex].destinationAirport + "(" + routes[routeIndex].destinationAirportId + ")");
    var url = "https://eng1003.monash/OpenFlights/airport/?id="+routes[routeIndex].destinationAirportId+"&callback=airportResponse";
   var script = document.createElement('script');
script.src = url;
document.body.appendChild(script);

    
    // TODO: Part 2 - Add code here to request airport information.
    //       The request should call the airportResponse function when successful.
}
